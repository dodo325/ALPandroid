package com.example.dodo0.alpandroid.nodeApi;

import android.app.Fragment;


public class NodeHelper {
    public NodeHelper() {
    }

    public Fragment getFragment(String locFile) {
        if (locFile.indexOf("b") == 0) return new TestNode(locFile);
        /*if (locFile.indexOf("a") ==0)*/
        return new HtmlNode(locFile);
    }
}
