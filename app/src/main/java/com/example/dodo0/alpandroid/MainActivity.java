package com.example.dodo0.alpandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button mOpenButton = (Button) findViewById(R.id.button_open_log_file);
        mOpenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_EDIT);
                Uri uri = Uri.parse(String.valueOf(getFileStreamPath("myLog.txt")));
                intent.setDataAndType(uri, "text/plain");
                startActivity(intent);
            }
        });
        Button mButton = (Button) findViewById(R.id.button_act);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LearnActivity.class));
            }
        });
        //stringFromJNI();  //c++
    }

    public native String stringFromJNI();
}
