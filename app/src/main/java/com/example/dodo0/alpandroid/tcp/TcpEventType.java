package com.example.dodo0.alpandroid.tcp;

public enum TcpEventType {
    CONNECTION_STARTED,
    CONNECTION_ESTABLISHED,
    CONNECTION_FAILED,
    CONNECTION_LOST,
    MESSAGE_RECEIVED,
    MESSAGE_SENT,
    DISCONNECTED
}
