package com.example.dodo0.alpandroid.nodeApi;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import com.example.dodo0.alpandroid.R;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;


public class TestNode extends Fragment {
    private WebView webView;
    private static final String LOG_TAG = "myLogHtmlNode";

    private String file = "a0.html";
    private EditText editAnswer;
    private Button sendAnswer;

    public TestNode(String str) {
        file = str;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.node_test, null);
        webView = (WebView) rootView.findViewById(R.id.wv);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl("file:///android_asset/" + file);
        saveLog("TestNode.loadUrl\t"+file);
        editAnswer = (EditText) rootView.findViewById(R.id.editText);
        sendAnswer = (Button) rootView.findViewById(R.id.sendAnswer);

        sendAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLog("TestNodeAnswer\t"+file+"\t"+editAnswer.getText());
            }
        });
        return rootView;
    }

    void saveLog(String string) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }

        String filename = "myLog.txt";
        FileOutputStream outputStream;
        Date currentTime = Calendar.getInstance().getTime();
        try {
            outputStream = getActivity().openFileOutput(filename, Context.MODE_APPEND);
            outputStream.write((currentTime + "\t" + string + "\n").getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}