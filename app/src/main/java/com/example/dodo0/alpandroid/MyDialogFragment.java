package com.example.dodo0.alpandroid;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;


public class MyDialogFragment extends DialogFragment {
    private static final String LOG_TAG = "myLogMyDialogFragment";
    ArrayAdapter<String> adapter;
    ArrayList<String> fileList;

    public String getItemText(int n){return fileList.get(n);}
    MyDialogFragment (ArrayList<String> adapter1){
        fileList = adapter1;
    }
    public interface ListDialogListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id);
        public void onPositiveClick(DialogInterface dialog);
        public void onNegativeClick(DialogInterface dialog);
    }

    ListDialogListener mListener;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (ListDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement ListDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater

        LayoutInflater inflater = getActivity().getLayoutInflater();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, fileList);

        LinearLayout v = (LinearLayout) inflater.inflate(R.layout.dialog1, null);
        builder.setView(v)
                .setPositiveButton(R.string.continu, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        mListener.onPositiveClick(dialog);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onNegativeClick(dialog);
                        // User cancelled the dialog
                    }
                });

        ListView listView = (ListView)v.findViewById(R.id.lv);

        AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mListener.onItemClick(parent,view,position,id);

            }
        };
        listView.setOnItemClickListener(onItemClickListener);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        return builder.create();
    }

}
