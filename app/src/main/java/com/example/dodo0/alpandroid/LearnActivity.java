package com.example.dodo0.alpandroid;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.example.dodo0.alpandroid.nodeApi.NodeHelper;
import com.example.dodo0.alpandroid.tcp.TcpClient;
import com.example.dodo0.alpandroid.tcp.TcpEvent;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class LearnActivity extends AppCompatActivity implements MyDialogFragment.ListDialogListener, Observer {
    private static final String LOG_TAG = "myLogLearnActivity";
    TcpClient client;
    MyDialogFragment dialog;
    FloatingActionButton fab_easy;
    FloatingActionButton fab_hard;
    android.app.FragmentTransaction fTrans;
    private Button button_next;
    private ArrayList<String> fileListServer = null;
    private boolean connected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        this.client = new TcpClient("192.168.1.40", 11719);
        this.client.addObserver(this);
        this.client.connect();

        // client.sendMessage("Hii!!");
         initLayout();

        saveLog("onCreate");
    }
    void initLayout() {
        button_next = (Button) findViewById(R.id.button_next);

        fileListServer = new ArrayList<>();
        fileListServer.add("a0.html");
        dialog = new MyDialogFragment(fileListServer);

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLog("button_next");
                dialog.show(getSupportFragmentManager(), "list");
                saveLog("onClick().button_next");
            }
        });
        fab_easy = (FloatingActionButton) findViewById(R.id.fab_easy);
        fab_easy.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Easy!?", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                saveLog("onClick().fab_easy");

            }
        });

        fab_hard = (FloatingActionButton) findViewById(R.id.fab_hard);
        fab_hard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hard!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                saveLog("onClick().fab_hard");
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        saveLog("LearnActivity.onResume()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.disconnect();
    }

    void saveLog(final String string) {
        Log.d(LOG_TAG, "saveLog." + string);
        if (connected) client.sendMessage(string);
        new Thread(new Runnable() {
            public void run() {

                String filename = "myLog.txt";
                FileOutputStream outputStream;
                Date currentTime = Calendar.getInstance().getTime();
                try {
                    outputStream = openFileOutput(filename, Context.MODE_APPEND);
                    outputStream.write((currentTime + "\t" + string + "\n").getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Snackbar.make(view, "Replace "+ ((TextView)view).getText(), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        setNode(((TextView)view).getText().toString());
        dialog.dismiss();

        saveLog("dialog.onItemClick\t" + position + "\t" + ((TextView) view).getText());
    }

    @Override
    public void onPositiveClick(DialogInterface dialo) {
        saveLog("dialog.onPositiveClick()");
        setNode(dialog.getItemText(0));
    }

    @Override
    public void onNegativeClick(DialogInterface dialog) {
        saveLog("dialog.onNegativeClick()");
    }
    void setNode(String locFile){
        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, new NodeHelper().getFragment(locFile));
        fTrans.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.learn_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            client.disconnect();
            client.connect();
        }
        return super.onOptionsItemSelected(item);//return true;
    }

    @Override
    public void update(Observable o, Object arg) {
        TcpEvent event = (TcpEvent) arg;
        Log.i(LOG_TAG, "TcpEvent: " + String.valueOf(event.getTcpEventType()));
        switch (event.getTcpEventType()) {
            case CONNECTION_FAILED:
                connected = false;
                break;
            case MESSAGE_RECEIVED:
                //Do something
                Log.i(LOG_TAG, "received: " + event.getPayload());
                break;
            case CONNECTION_ESTABLISHED:
                connected = true;
                String android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
                client.sendMessage("iam:" + android_id);
                runOnUiThread(new Runnable() {
                    public void run() {
                        //Update ui
                        //TODO: set Node setNode();
                    }
                });
                break;
        }
    }
}
