package com.example.dodo0.alpandroid.tcp;

public enum TcpClientState {
    DISCONNECTED,
    CONNECTING,
    CONNECTED,
    CONNECTION_STARTED,
    FAILED
}
