package com.example.dodo0.alpandroid.nodeApi;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.dodo0.alpandroid.R;

public class VideoNode extends Fragment {
    private static final String LOG_TAG = "myLogHtmlNode";
    private VideoView video;
    private String path1 = "a0.html";
    public VideoNode(String uri){
        path1 = uri;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.node_html, null);
        Uri uri=Uri.parse(path1);

        video=(VideoView)rootView.findViewById(R.id.vv);
        video.setVideoURI(uri);
        video.setMediaController(new MediaController(getActivity()));
        video.requestFocus(0);
        video.start();

        return rootView;
    }
}
