package com.example.dodo0.alpandroid.nodeApi;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.dodo0.alpandroid.R;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

public class HtmlNode extends Fragment {
    private WebView webView;
    private static final String LOG_TAG = "myLogHtmlNode";

    private String file = "a0.html";
    public HtmlNode(Uri uri){
        file = uri.toString();
    }
    boolean ff=true;
    public HtmlNode(String str){
        file = "file:///android_asset/"+str;ff=false;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.node_html, null);
        webView = (WebView) rootView.findViewById(R.id.wv);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.setWebViewClient(new MyAppWebViewClient());
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl(file);
        saveLog("Node.loadUrl\t"+file);

        return rootView;
    }
    public class MyAppWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(Uri.parse(url).getHost().length() == 0) {
                return false;
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
            return true;
        }

    }
    void saveLog(final String string){
        new Thread(new Runnable() {
            public void run() {
                if (!Environment.getExternalStorageState().equals(
                        Environment.MEDIA_MOUNTED)) {
                    Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
                    return;
                }

                String filename = "myLog.txt";
                FileOutputStream outputStream;
                Date currentTime = Calendar.getInstance().getTime();
                try {
                    outputStream = getActivity().openFileOutput(filename, Context.MODE_APPEND);
                    outputStream.write((currentTime + "\t" + string + "\n").getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
