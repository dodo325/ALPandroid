import socket

serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
serv_sock.bind(('', 11719))
serv_sock.listen(10)

while True:
    # Бесконечно обрабатываем входящие подключения
    client_sock, client_addr = serv_sock.accept()
    print('Connected by', client_addr)

    while True:
        # Пока клиент не отключился, читаем передаваемые 
        # им данные и отправляем их обратно
        data = client_sock.recv(128)
        print("Data:", data)
        client_sock.send(b"Hello?!")
    client_sock.close()